Salut Ugo, voici ma TodoList en PHP 7 
Framework Codigniter 4
Login: admin@admin.fr
Password: admin
Virtual Host: todolist-iceb.loc

<VirtualHost *:80>
	ServerName todolist-iceb.loc
	DocumentRoot <directory>www/todolist/public/
	<Directory  "<directory>www/todolist/public/">
		Options +Indexes +Includes +FollowSymLinks +MultiViews
		AllowOverride All
		Require local
	</Directory>
</VirtualHost>


Connexion sur http://todolist-iceb.loc/

Merci !