<?php namespace App\Controllers;

class Home extends BaseController
{
  public function __construct() {
    $session = session();
    
    if (!($session->get('logged_in'))) return redirect()->to('/home/login_verification');
  }
	public function index()
	{
    helper('form');
    $session = session();
    
    if (!($session->get('logged_in'))) return redirect()->to('/home/login_verification');
		//SELECT * FROM `task` LEFT JOIN `categorie` ON `categorie`.`CA_id` = `task`.`TA_id` 
		$db = db_connect();
		$builder = $db->table('task');
		$builder->join('categorie', 'categorie.CA_id = task.TA_id_categorie', 'left');
    $builder->groupBy('TA_id');
    if (isset($_GET['orderCat'])) $builder->orderBy('TA_id_categorie ASC');
    if (isset($_GET['orderName'])) $builder->orderBy('TA_titre ASC');
    if (isset($_GET['orderDate'])) $builder->orderBy('TA_id ASC');
		$builder->orderBy('TA_etat ASC, TA_id ASC');
    $query   = $builder->get(); 
    
    $cat = $db->table('categorie');
    $queryCat   = $cat->get(); 

		$data = ['task' => $query->getResult(), 'categorie' => $queryCat->getResult()];
		return view('toDoList', $data);
	}
	public function editTask()
	{
    $values = $_POST;
    d($_POST);

    $db = db_connect();
    $build_task = $db->table('task');
    
    $inputTask = array (
      'TA_titre' => $values['titreTask'],
      'TA_id_categorie' => $values['id_categorie'],
      'TA_etat' => '0',
      'TA_date_crea' =>  date('Y-m-d H:i:s'),
    );

    if ((isset($values['addCategorie'])) && (!empty($values['addCategorie']))) {
      $inputCategorie = array (
        'CA_titre' => $values['addCategorie'],
      );

      $build_cat = $db->table('categorie');
      $build_cat->insert($inputCategorie);
      $query = $db->query("SELECT * FROM categorie ORDER BY CA_ID DESC LIMIT 1");
  
      $row = $query->getRow();
      $insert_id = $row->CA_id;

      $inputTask['TA_id_categorie'] = $insert_id;
      echo $insert_id;
    }
		if (!empty($values['id_task'])) {
      $build_task->where('TA_id', $values['id_task']);
      $build_task->update($inputTask);
    }else{
      $build_task->insert($inputTask);
    }


    return redirect()->to('/home/index');
  }
  public function doneTask($id=false)
	{
    if($id) {
      $db = db_connect();
      $query = $db->query("SELECT * FROM task where TA_id = $id");
  
      $row = $query->getRow();
  
  switch ($row->TA_etat) {
    case '0':
      $etat = '1';
    break;
    case '1':
      $etat = '0';
    break;
      
  }
      $build_task = $db->table('task');
  
      $build_task->where('TA_id', $id);
      
      $build_task->update(array('TA_etat' =>$etat));
    }
    return redirect()->to('/home/index');
  }

	public function deleteTask($id=false)
	{
    $db = db_connect();
    $build_task = $db->table('task');

    if($id) {
      $build_task->where('TA_id', $id);
      $build_task->delete();
    }
    return redirect()->to('/home/index');
  }
  //--------------------------------------------------------------------
  
  function login_verification() {
    helper('form');
    if (empty($_POST)) return view('login');
    $post = $_POST;
    $db = db_connect();
    $loginBuilder = $db->table('user');
    $query = $loginBuilder->getWhere([
      'US_email' => $post['email'],
      'US_password' => hash('sha256',$post['password'])
    ]);
    $user = $query->getResult();
    if (count($user) > 0) {
      $this->session_init($query->getRow());
      return redirect()->to('/home/index');
      
    }else{
      return redirect()->to('/home/login_verification?error=id');

    }
}
function session_init ($user) {
  $session = session();


      $session_data = array(
          'logged_in' => TRUE,
          'email' => $user->US_email,
        );
        $session->set($session_data);
        return $session_data;
  }

    public function deconnexion () {

      $session = session();
      $session->stop();
      $session->destroy();
      return redirect()->to('/home/login_verification');
      

    }    

}
