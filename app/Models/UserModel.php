<?php
 namespace App\Models;

 use CodeIgniter\Model;
 
 class UserModel extends Model
 {
    function getTasks() {
     		//SELECT * FROM `task` LEFT JOIN `categorie` ON `categorie`.`CA_id` = `task`.`TA_id` 
		$db = db_connect();
		$builder = $db->table('task');
		$builder->join('categorie', 'categorie.CA_id = task.TA_id_categorie', 'left');
    $builder->groupBy('TA_id');
    if (isset($_GET['orderCat'])) $builder->orderBy('TA_id_categorie ASC');
    if (isset($_GET['orderName'])) $builder->orderBy('TA_titre ASC');
    if (isset($_GET['orderDate'])) $builder->orderBy('TA_id ASC');
		$builder->orderBy('TA_etat ASC, TA_id ASC');
    $query   = $builder->get(); 

    $cat = $db->table('categorie');
    $queryCat   = $cat->get(); 

      return $query->getResult();
    }
    function getCategorie() {
      $cat = $db->table('categorie');
      $queryCat   = $cat->get(); 
        return $queryCat->getResult();
    }
 }