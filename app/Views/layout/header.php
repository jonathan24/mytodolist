<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/bootstrap.min.css" >
    <title>ToDo List Jonathan</title>
  </head>
  <body>
    <div class="container">  
      <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand">My ToDo List</a>
        <div class="row">
          <?php if (isset($_SESSION['logged_in'])  ) {  ?>
            <div class="col"><?php  echo anchor('home/login_verification', $_SESSION['email'],'class="h6 float-right" ');  ?></div>
  
            <div class="col"><?php  echo anchor('home/deconnexion', 'Logout','class="h6 float-right" ');  ?></div>
          <?php }else{ ?>
            <div class="col"><?php  echo anchor('home','login','class="h6 float-right" ') ;  ?></div>
          <?php }  ?>
            
        </div>
          <?php if (isset($_SESSION['logged_in'])  ) {  ?>
        <div class="h3 row">
         <div class="position-relative col">
            <a  href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <ion-icon name="options-outline"></ion-icon>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <?php echo anchor('home/index?orderCat=asc', 'Trier par catégorie','class="dropdown-item"'); ?>
            <?php echo anchor('home/index?orderName=asc', 'Trier par nom','class="dropdown-item"'); ?>
            <?php echo anchor('home/index?orderDate=asc', 'Trier par date','class="dropdown-item"'); ?>
            <?php echo anchor('home/index', 'Trier par tache','class="dropdown-item"'); ?>
            </div>
          </div>
        </div>
        <?php }  ?>

      </nav>
    </div>