<?php echo view('layout/header');  ?>
<div class="container">

<div class="col-6 m-auto">
  <?php if (isset($_GET['error'])) :  ?>
    <div class="alert alert-danger">
      Login ou mot de passe incorrecte !
    </div>
  <?php endif;  ?>
  </p>
  <?php if (isset($_SESSION['logged_in'])  ) { ?>
    <h3 class="text-center py-3">Bienvenue <?php echo $_SESSION['email'] ;?></h3>
    <h2><?php echo anchor('home/index/', 'Aller sur ma Todo Liste =>'); ?></h2>

    <?php }else{ ?>
      <h3 class="text-center py-3">Identification</h3>
      <?php echo form_open('home/login_verification',array('class'=>'')); ?>

      <div class="form-group">
        <label for="InputEmail1">Adresse Mail</label>
        <?php echo form_input('email', set_value('email'), ' class="form-control" placeholder="Adresse Email" required="required" ','email'); ?>
      </div>
      <div class="form-group">
        <label for="InputPassword1">Mot de Passe</label>
        <?php echo form_password('password', '', '  class="form-control" placeholder="Password"  required="required"'); ?>
      </div>

      <div class="text-center"><button type="submit" class="btn btn-primary btn-lg">Connexion</button></div>
      <?php echo form_close(); ?>
    <?php } ?>

</div>
  <?php echo view('layout/footer'); ?>
</div>