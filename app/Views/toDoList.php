<?php echo view('layout/header');  ?>
<div class="container" >    
  <ul class="list-group mt-4">
  <?php foreach ($task as $row) : ?>
  <li class="list-group-item <?php if ($row->TA_etat == '1') echo "list-group-item-dark taskDone"; ?>">
    <input type="checkbox" name="task-1" class="mr-3" <?php if ($row->TA_etat == '1') echo 'checked'; ?> onClick="location.href='/home/doneTask/<?php echo $row->TA_id; ?>';">
    <span class="h4"><?php	echo $row->TA_titre;?></span>
      <div class="float-right">
        <span class="categorie h4">
          <button type="button" class="btn btn-outline-primary"><?php	echo $row->CA_titre ; ?></button>
        </span>
        <span class="controls align-bottom">
          <a href="#" data-toggle="modal" data-target="#TacheForm"  onClick="$('.idTask').val('<?php echo $row->TA_id;?>'); $('#ajouterUneTacheInput').val('<?php echo $row->TA_titre;?>'); $('#selectCategorie').val('<?php echo $row->TA_id_categorie;?>');"><ion-icon name="create-outline" size="large"></ion-icon></a>
          <?php echo anchor('home/deleteTask/'.$row->TA_id, '<ion-icon name="trash-outline" size="large"></ion-icon>'); ?>
        </span>
      </div>
    </li>
    <?php	 endforeach ; ?>
  </ul>
</div>
<div class="text-center pt-3 fixed-bottom m-auto w-25" >   
  <a href="#" data-toggle="modal" data-target="#TacheForm"  onClick="$('.idTask').val('')"><ion-icon name="add-circle-outline" class="display-1" ></ion-icon></a>
</div>
<!-- Modal -->
<div class="modal fade" id="TacheForm" tabindex="-1" role="dialog" aria-labelledby="TacheFormTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TacheFormTitle">Votre tâche</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('home/editTask');
        echo form_input('id_task', (isset($row->TA_id) ? $row->TA_id : ''),'class="idTask invisible"');
      ?>
      
      <div class="modal-body">
          <div class="form-group">
            <label for="ajouterUneTacheInput">Ajouter une tâche</label>
            <input type="text" class="form-control" id="ajouterUneTacheInput" placeholder="Nouvelle tâche" name="titreTask" value="">
          </div>
          <label class="my-1 mr-2" for="selectCategorie">Catégorie</label>
            
            <select class="custom-select my-1 mr-sm-2" id="selectCategorie" name="id_categorie">
              <?php foreach ($categorie as $catRow) : ?>
                <option value="<?php	echo $catRow->CA_id;?>" ><?php	echo $catRow->CA_titre;?></option>
              <?php	 endforeach ; ?>
            </select>
            <p><a href="javascript:void(0);" onClick="$('.inputAddCategorie').toggleClass('invisible');" ><ion-icon name="add-circle-outline" size="large" class="align-middle mr-2 "></ion-icon>Ajouter une catégorie</a></p>
            <input class="form-control inputAddCategorie invisible" type="text" name="addCategorie">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
        <?php echo form_submit('enregistrer', 'Enregistrer','class="btn btn-primary"');?>
      </div>
      <?php echo form_close(); ?>

    </div>
  </div>
</div>
<style>
.taskDone {
  text-decoration: line-through;
}
</style>
<?php echo view('layout/footer'); ?>
