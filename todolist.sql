-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 09 déc. 2020 à 12:09
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `todolist`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `CA_id` int(255) NOT NULL AUTO_INCREMENT,
  `CA_titre` varchar(255) NOT NULL,
  PRIMARY KEY (`CA_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`CA_id`, `CA_titre`) VALUES
(1, 'Travail'),
(2, 'Maison'),
(3, 'Urgent');

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

DROP TABLE IF EXISTS `task`;
CREATE TABLE IF NOT EXISTS `task` (
  `TA_id` int(255) NOT NULL AUTO_INCREMENT,
  `TA_titre` varchar(255) NOT NULL,
  `TA_id_categorie` int(11) NOT NULL,
  `TA_etat` tinyint(5) NOT NULL,
  `TA_date_crea` datetime NOT NULL,
  PRIMARY KEY (`TA_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `task`
--

INSERT INTO `task` (`TA_id`, `TA_titre`, `TA_id_categorie`, `TA_etat`, `TA_date_crea`) VALUES
(1, 'Tache 1', 1, 0, '2020-12-09 13:08:56'),
(2, 'Tache 2', 2, 0, '2020-12-09 13:09:04'),
(3, 'Tache 3', 3, 0, '2020-12-09 13:09:14');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `US_id` int(255) NOT NULL AUTO_INCREMENT,
  `US_email` varchar(255) NOT NULL,
  `US_password` varchar(255) NOT NULL,
  PRIMARY KEY (`US_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`US_id`, `US_email`, `US_password`) VALUES
(1, 'admin@admin.fr', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
